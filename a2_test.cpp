// a2_test.cpp

/////////////////////////////////////////////////////////////////////////
//
// Student Info
// ------------
//
// Name : <put your full name here!>
// St.# : <put your full SFU student number here>
// Email: <put your SFU email address here>
//
//
// Statement of Originality
// ------------------------
//
// All the code and comments below are my own original work. For any non-
// original work, I have provided citations in the comments with enough
// detail so that someone can see the exact source and extent of the
// borrowed work.
//
// In addition, I have not shared this work with anyone else, and I have
// not seen solutions from other students, tutors, websites, books,
// etc.
//
/////////////////////////////////////////////////////////////////////////

#include "a2.h"
#include "cmpt_error.h"
#include <iostream>

// You can #include other standard C++ files. But do *not* include any
// non-standard C++ files.

using namespace std;

//
// Put your test functions here (one test function for each function in a2.h),
// and then call them in main().
// 

int main() {
  cout << "Assignment 2\n";

  //
  // ... call your test functions here ...
  //
}
