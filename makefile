####################################################################
#
# Makefile for CMPT 135 Spring 2021, SFU Surrey.
#
#####################################################################

# Set the C++ compiler options:
#   -std=c++17 compiles using the C++17 standard (or at least as 
#    much as is implemented by the compiler, e.g. for g++ see
#    http://gcc.gnu.org/projects/cxx0x.html)
#   -Wall turns on all warnings
#   -Wextra turns on even more warnings
#   -Werror causes warnings to be errors 
#   -Wfatal-errors stops the compiler after the first error
#   -Wno-sign-compare turns off warnings for comparing signed and 
#    unsigned numbers
#   -Wnon-virtual-dtor warns about non-virtual destructors
#   -g puts debugging info into the executables (makes them larger)
CPPFLAGS = -std=c++17 -Wall -Wextra -Werror -Wfatal-errors -Wno-sign-compare -Wnon-virtual-dtor -g

a2_graph: a2_graph.o
	g++ a2_graph.o -o a2_graph -lsfml-graphics -lsfml-window -lsfml-system

a2_graph.o: a2_graph.cpp
	g++ $(CPPFLAGS) -c a2_graph.cpp

submit:
	rm -f a2_submit.zip
	zip a2_submit.zip a2.h a2_test.cpp a2_graph.cpp

clean:
	rm -f a2_test
	rm -f a2_graph.o a2_graph
