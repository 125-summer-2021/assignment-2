# Assignment 2: A Dynamic Array

In this assignment your task is to implement, and test, a set of functions
that implement a dynamic array. The basic idea of a dynamic array is that it
is a regular array divide into a left part and a right part. The left part
contains the data the user can access, and the right part contains unused
locations that get used when the size of the array increases.


## Getting Started

Please download all the files in this repository (hint: you can download them
as a `.zip` file using the button near the top of the repository page). Here
are the important assignment files:

- [int_list.h](int_list.h) contains the `int_list` struct you'll need in the
  questions below. **Important**: Don't change [int_list.h](int_list.h) in any
  way!

- [a2.h](a2.h) is where you write your solutions for questions 1 to 13. It is
  set up to get you started. Make sure to fill in the info in the header at
  the top.

  - You **can** `#include` any other *standard* C++ header files, but **not**
    `vector` or any other file that provides a container class. `int_list`
    should only use an array to store/process its data.

    Do *not* include any non-standard header files.

  - You **can** add any helper functions you think might be useful (as long as
    you write them yourself). Only include code you wrote yourself.

- [test_small.txt](test_small.txt) contains the sample data used below.

- [a2_test.cpp](a2_test.cpp) is where you write testing functions for each of
  the requested functions. It should have a `main()` function that calls all
  the test functions. See below for example of how to write test functions.

- [a2_graph.cpp](a2_graph.cpp) is where you write the code for question 14. It
  should have a `main()` function that uses SFML to draw the requested bar
  chart.

- [data11.txt](data11.txt), [data22.txt](data22.txt), and
  [data100.txt](data100.txt) are sample data files read by
  [a2_graph.cpp](a2_graph.cpp).

- [makefile](makefile) is the makefile for this assignment.

- [cmpt_error.h](cmpt_error.h) contains the `cmpt::error` function.


### Testing

For each function requested by questions 1 to 13 below, also write a *test
function* that *automatically* checks that the function is working correctly.
Name these test functions the same as the function being tested, but with
`_test` added to the end. For example, the testing function for `make_new`
should be called `make_new_test`, and might be implemented like this:

```cpp
// a2_test.cpp

#include <cassert>

// ...

void make_new_test() {
   cout << "Running make_new_test() ... ";

   int_list a = make_new();
   assert(a.size == 0);
   assert(a.capacity == 5);

   // ...

   cout << "\nmake_new_test() passed\n";
}
```

Each test function should have 5 or more different test cases.


## Questions

Make sure to use **exactly** the same function headers for all the questions,
otherwise the marking software will probably give you 0!

1. In `a2.h`, write a function that makes a new empty `int_list` of size 0 and
   capacity `cap`. It has this header:

   ```cpp
   int_list make_new(int cap = 5)
   ```

   For example, calling `make_new(50)` returns a new `int_list` of size 0 and
   capacity 50. Calling `make_new()` returns a new `int_list` of size 0 and
   capacity 5.

   Remember to create a function called `make_new_test()` to test that
   `make_new` works correctly.

   If the capacity value passed to `make_new` is 0 or less, then call
   `cmpt::error` with a helpful message.


2. In `a2.h`, implement this function:

   ```cpp
   void deallocate(int_list& arr)
   ```

   `deallocate(arr)` de-allocates the underlying array that `arr` points
   to.

   `deallocate(arr)` should call `delete[]` on the underlying array, and also
   set the array pointer to `nullptr`. Setting the array pointer to `nullptr`
   prevents memory corruption in cases when `deallocate` is called more than
   once on the same array.

   It is up to the programmer to always remember to call `deallocate` when
   they are finished using an `int_list`.


3. In `a2.h`, implement this function:

   ```cpp
   int_list copy(const int_list& other)
   ```
   
   `copy(other)` returns a new `int_list` that is a copy of `other`. The
   returned `int_list` points to its own array that is a copy of the array in
   `other`.


4. In `a2.h`, implement this function:

   ```cpp
   string to_str(const int_list& arr)
   ```

   The string returned by `to_str(arr)` begins with a `{` and ends with `}`.
   The values are separated by ", ".

   For example, if `arr` is an array with the values 8, 8, and -4, then
   `to_str(arr)` returns this string:

   ```cpp
   {8, 8, -4}
   ```

   If `arr.size` is 0, then `"{}"` is returned. If `arr.size == 1`, then
   there's only one value in it (and no commas).

   **Important**: Make sure the string returned by `to_str` follows exactly
   this format. The spacing should be *exactly* as in the examples.

   Note that `to_str` only writes the first `size` elements of the underlying
   array.

   After you've implemented `to_str`, use it to (easily!) implement these two
   functions:

   ```cpp
   // writes to_str(arr) to cout
   void write(const int_list& arr)

   // same as write, but also writes '\n' at the end
   void writeln(const int_list& arr)
   ```

5. In `a2.h`, implement this function:

   ```cpp
   int get(const int_list& arr, int i)
   ```
   
   `get(arr, i)` returns the `int` at index location `i` of the underlying
   array.

   `get` *checks the bounds* of `i`, i.e. if `i < 0` or `i >= arr.size`, then
   call `cmpt::error` to stop the program with a helpful error message.

   Next, implement the `set(arr, i, n)` function, which assigns `int` `n`
   to location `i` of the underlying array:

   ```cpp
   void set(int_list& arr, int i, const int& n)
   ```

   As with `get`, `set` checks the bounds of `i`, i.e. if `i < 0` or `i >=
   arr.size`, then call `cmpt::error` to stop the program with a helpful error
   message.


6. In `a2.h`, implement this function: 

   ```cpp
   void append_right(int_list& arr, int n)
   ```

   `append_right(arr, n)` adds a new `int` to the right end of `arr`,
   re-sizing `arr` if necessary.

   If `arr.size < arr.capacity`, then `append_right` increments `arr.size` and
   puts `n` at the first unused location at the right end of `arr`.

   Otherwise, if `arr.size >= arr.capacity`,  the underlying array is first
   re-sized to be **twice the capacity**. Then `n` is added to the right end
   of the array as in the previous case.


7. In `a2.h`, implement this operator:

   ```cpp
   bool operator==(int_list a, int_list b)
   ```
   
   `a` and `b` are considered equal if they have the same size, and contain
   the same values in the same order.

   Also, implement this operator:

   ```cpp
   bool operator!=(int_list a, int_list b)
   ```

   `a != b` is true just when `a` and `b` are *not* the same.


8. In `a2.h`, implement this function:

   ```cpp
   void clear(int_list& arr)
   ```

   After calling `clear(arr)`, the size of `arr` is 0. The capacity of the
   underlying array doesn't need to change.


9. In `a2.h`, implement this function:

   ```cpp
   void append_left(int_list& arr, int n)
   ```

   It works just like `append_right` from above, but `n` is added at the left
   end, i.e as the *first* element of the array (and all the other elements
   are moved right one position).

   For example, if `arr` is `{7, 4}`, then after calling `append_left(arr, 5)`
   `arr` will be `{5, 7, 4}`.


10. In `a2.h`, implement this function:

    ```cpp
    void shrink_to_fit(int_list& arr)
    ```

    `shrink_to_fit(arr)` will, if possible, re-size the underlying array so
    the capacity is the same as the size. The values in `arr` should remain
    the same, and in the same locations, after calling `shrink_to_fit`.


11. In `a2.h`, implement this function:
  
    ```cpp
    int_list make_fromFile(const string& fname)
    ```

    `fname` is the name of a text file, and `make_fromFile(fname)` reads all
    the values, in the same order they occur in the file, into a new
    `int_list`. To read the values, use a loop that repeatedly calls `fin >>
    n`, where `fin` is an `fstream` object and `n` is the value read in.

    For example, the text file [test_small.txt](test_small.txt) contains these
    numbers:

    ```
    4 -2 0
    8000 64 64 -125
    ```

    Then the code

    ```cpp
    int_list arr = make_fromFile("test_small.txt");
    cout << arr.size << " numbers\n";
    writeln(arr);
    ```

    prints:

    ```
    7 numbers
    {4, -2, 0, 8000, 64, 64, -125}
    ```


12. In `a2.h`, implement this function:

    ```cpp
    double average(const int_list& arr)
    ```

    `average(arr)` returns the average of the values in `arr`. If `arr` is
    empty, 0 is returned.


13. In `a2.h`, implement this function:

    ```cpp
    int_list concat(int_list a, int_list b)
    ```

    `concat(a, b)` returns a new `int_list` consisting of the elements of `a`
    followed by the elements of `b`. For example, if `a` is `{2, 1, 6}` and
    `b` is `{6, 5, 2, 4}`, then `concat(a, b)` returns the new `int_list` `{2,
    1, 6, 6, 5, 2, 4}`.

    The input lists `a` and `b` (and their underlying arrays) should *not* be
    changed.


14. In `a2_graph.cpp`, implement an SFML program (with its own `main()`) that,
    when run, reads in the name of a data file that has 10 to 100 non-negative
    integers in it. The integers must all be 0 or greater, and all 499 or
    less.

    When `a2_graph.cpp` runs, it draws a bar chart in a window, with one
    rectangle per number. The heights of the rectangles should be the same as
    number in the file. The width of the rectangles should all be the same,
    and the rectangles should be drawn in the same order as the numbers in the
    data file.

    The marker will be looking for the following:

    1. The dimensions of the graph window are 500 pixels wide and 500 pixels
       high.

    2. You have used code from `a2.h` to read in and store all the numbers.

    3. If the file has 0 numbers, or if it has more than 100 numbers, then the
       program **doesn't draw a graph**, and instead prints a friendly and
       helpful error message.

    4. If any of the numbers in the file are less than 0 or more than 499,
       then the program **doesn't draw a graph**, and instead prints a
       friendly and helpful error message.

    5. The bars are drawn to fit neatly in the available space. There should
       be no very large asymmetric gaps at the beginning or end of the chart.
       Also, the bars are easily legible, e.g. they are filled in with a color
       that is easy to see.

    Here are some sample runs. 

    Using the data file [data11.txt](data11.txt), the terminal command
    `./a2_graph data11.txt` draws this graph:

    ![11 bars](data11_chart.png)

    Using the data file [data22.txt](data22.txt), the terminal command
    `./a2_graph data22.txt` draws this graph:

    ![22 bars](data22_chart.png)

    Using the data file [data100.txt](data100.txt), the terminal command
    `./a2_graph data100.txt` draws this graph:

    ![100 bars](data100_chart.png)


## Submit Your Work

Please submit your work on Canvas in a single .zip file named `a2_submit.zip`
that contains just these files: `a2.h`, `a2_test.cpp`, and `a2_graph.cpp`.

The assignment [makefile](makefile) will create this for you if you type `make
submit`, e.g.:

```
$ make submit
rm -f a2_submit.zip
zip a2_submit.zip a2.h a2_test.cpp a2_graph.cpp
  adding: a2.h (deflated 56%)
  adding: a2_test.cpp (deflated 55%)
  adding: a2_graph.cpp (deflated 6%)
```

**Do not submit** [cmpt_error.h](cmpt_error.h), **or any other files**. A copy
of [cmpt_error.h](cmpt_error.h) and [int_list.h](int_list.h) will be put in
the same folder as your `.cpp` files when the marker runs your programs.

For questions 1 to 13, the marker will run the terminal command `make -B
a2_test` to compile your program, using the [makefile for this
assignment](makefile). For question 14, the marker will run the terminal
command `make -B a3_graph` to compile and link [a2_graph.cpp](a2_graph.cpp),
using the [makefile for this assignment](makefile).

Note that `make -B` forces the file to be recompiled every time. If you leave
out the `-B` option, then changing `a2.h` can cause `a2_test.cpp` and
`a2_graph.cpp` to *not* be re-compiled.


## Basic Requirements

Before we give your program any marks, it must meet the following basic
requirements:

- The requested files must compile on Ubuntu Linux using [the makefile for
  this assignment](makefile).

  If your program fails to compile, your mark for this assignment will be 0.

- `a2_test` has no memory leaks, according to `valgrind`, e.g.:

  ```
  $ valgrind ./a2_test
    
  // ... lots of output ... 
  ```

  A program is considered to have no memory leaks if:

  - In the `LEAK SUMMARY`, `definitely lost`, `indirectly lost`, and
    `possibly lost` must all be 0.

  - `ERROR SUMMARY` reports 0 errors.

  - It is usually okay if **still reachable** reports a non-zero number of
    bytes.

  If `valgrind` reports any errors for your program, your mark for the
  assignment will be 0.

- **All the files that you write code for must include the properly filled-in
  statement of originality at the top of the file**. If one or more of these
  headers are not properly filled out, then we will assume your work is not
  original, and it will not be marked.
  
If your program meets all these basic requirements, then it will graded using
the marking scheme below.


## Marking Scheme

### Source Code Readability (6 marks)

- All names of variables, functions, structs, classes, etc. are sensible,
  self-descriptive, and consistent.

- Indentation and spacing is perfectly consistent and matches the structure of
  the program. All blank lines and indents should have a reason.

- All lines are 100 characters in length, or less.

- Comments are used when appropriate: to describe code that is tricky or very
  important. There are no unnecessary comments, and no commented-out code.

- Appropriate features of C++ are used in an appropriate way. For example, do
  **not** use a feature of C (like C-style strings) when there is a better
  feature of C++ (e.g. the standard `string` class). Don't use any features
  you don't understand.

- Overall, the source code is easy to read and understand.


### Questions (31 marks)

Questions 1 to 13 (26 marks):

- 1 mark for correctly and efficiently implementing the functions for each
  question.

- 1 mark for testing the functions for each question with their own test
  function.

Question 14 (5 marks):

- 1 mark for each of the numbered points given at the end of the question.


### Following Submission Instructions (1 mark)

- The submitted `.zip` file has the proper name, and contains exactly the
  requested files.
